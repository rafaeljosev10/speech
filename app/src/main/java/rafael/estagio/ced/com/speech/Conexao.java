package rafael.estagio.ced.com.speech;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static rafael.estagio.ced.com.speech.MainActivity.REQUEST_PERMISSION_CODE;

public class Conexao extends AsyncTask<String,String,String> {

    private ProgressDialog progesDialog;
    protected ProgressDialog progressDialog;
    private Context asyncContext;
    private String login, senha, descricao;

    public Conexao (Context context) {
        asyncContext = context;

//        //Conexao com o SQLite
//        try {
//            dataBase = new DataBase(context);
//            conn = dataBase.getWritableDatabase();
//            repositorioBD = new RepositorioBD(conn);
//
//        } catch (SQLException ex) {
//            token("Erro ao criar a conexão com o  banco" + ex.getMessage(), "Erro");
//            Log.e("fudeu", "time");
//
//        }
    }

    // Na thread principal
    @Override
    protected void onPreExecute() {
        progesDialog = ProgressDialog.show(asyncContext, "Aguarde", "Dando o gás aqui", true);
        progesDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {
        StringBuffer response;

        //Passagem dos dados
        login = params[1];
        senha = params[2];
        descricao = params[3];

        if(isOnline()) {
            try {
                URL url = new URL("https://speech.googleapis.com/v1/speech:recognize"); //Enter URL here

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id_telefone", params[0]);
                jsonObject.put("_id", params[1]);
                jsonObject.put("senha", md5(params[2]));
                jsonObject.put("f875ce6e9518fafd2fd4e0de710c8a24", "935547b06c3f8dd7efebc41f428ee7d1");

                String mensagem = jsonObject.toString();

                Log.w("JSON", mensagem);

                //Define a conexao com o servidor
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setFixedLengthStreamingMode(mensagem.getBytes().length);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

                //Abrindo Conexão
                connection.connect();

                //envia os dados
                OutputStream outputStream = new BufferedOutputStream(connection.getOutputStream());
                outputStream.write(mensagem.getBytes());
                outputStream.flush();

                //Recebe resposta do servidor
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null) {
                    response.append(inputLine);
                }
                bufferedReader.close();
                Log.i("HTTPRESPONSE", response.toString());

                return response.toString();

            } catch (IOException | JSONException e) {
                System.err.println("ERRO!");
                return "error";
            }
        }
        else{
            return "\"404";
        }
    }

    @Override
    protected void onPostExecute(String aVoid) {
        progesDialog.dismiss();
//        if(check_response(aVoid))
//            inserir();
    }

    //Método md5
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) asyncContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ContextCompat.checkSelfPermission((Activity) this.asyncContext, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this.asyncContext, Manifest.permission.RECORD_AUDIO)) {
                callDiallog("", new String[]{Manifest.permission.ACCESS_NETWORK_STATE});
            } else {
                ActivityCompat.requestPermissions((Activity) this.asyncContext, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, REQUEST_PERMISSION_CODE);
            }
        } else {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }

        return false;
    }

    private void callDiallog(String mensage, final String[] permission) {

        new AlertDialog.Builder(asyncContext)
                .setTitle("Permission")
                .setMessage(mensage)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) asyncContext,permission,REQUEST_PERMISSION_CODE);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
}
