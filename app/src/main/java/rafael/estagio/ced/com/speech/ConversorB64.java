package rafael.estagio.ced.com.speech;

import android.util.Base64;

public class ConversorB64 {

    public byte[] converte64(String testValue){
        return Base64.encode(testValue.getBytes(), Base64.DEFAULT);
    }

    public byte[] converteString(String testValue){
        return Base64.decode(testValue, Base64.DEFAULT);
    }
}
