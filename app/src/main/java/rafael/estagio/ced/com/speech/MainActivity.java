package rafael.estagio.ced.com.speech;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;


public class MainActivity extends AppCompatActivity {

    TextToSpeech falador;
    EditText edtDoMeio;
    ImageButton imbQueFala;
    ImageButton imbQueEscuta;
    public static final int REQUEST_PERMISSION_CODE = 128;


    private VoiceRecorder mVoiceRecorder;
    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        // quando começa a falar
        @Override
        public void onVoiceStart() {
//            showStatus(true);
//            if (mSpeechService != null) {
//                mSpeechService.startRecognizing(mVoiceRecorder.getSampleRate());
//            }
        }

        // enquanto fala
        @Override
        public void onVoice(byte[] data, int size) {
//            if (mSpeechService != null) {
//                mSpeechService.recognize(data, size);

                Log.i("voz",""+size);

//            }
        }

        // quando a garvação é terminada
        @Override
        public void onVoiceEnd() {
//            showStatus(false);
//            if (mSpeechService != null) {
//                mSpeechService.finishRecognizing();
//            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtDoMeio = (EditText) findViewById(R.id.edtDoMeio);
        imbQueFala = (ImageButton) findViewById(R.id.imbQueFala);
        imbQueEscuta = (ImageButton) findViewById(R.id.imbQueescurta);

        imbQueEscuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificaPermissao();

            }
        });

     /**
        falador = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    falador.setLanguage(Locale.getDefault());
                }

            }
        });

        imbQueFala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toSpeak = edtDoMeio.getText().toString();
                falador.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });

        imbQueEscuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                promptSpeechInput();
            }
        });
     **/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE:
                for (int i = 0 ; i < permissions.length ; i++  ) {
                    if(permissions[i].equalsIgnoreCase(Manifest.permission.RECORD_AUDIO)
                            && grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        startVoiceRecorder();
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void verificaPermissao() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                callDiallog(getString(R.string.mensagemGravacao), new String[]{Manifest.permission.RECORD_AUDIO});
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_PERMISSION_CODE);
            }
        } else {
            startVoiceRecorder();
        }
    }

    // inicia a gravação
    private void startVoiceRecorder() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
        }
        mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
        mVoiceRecorder.start();
    }

    // para a gravação
    private void stopVoiceRecorder() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
        }
    }

    /**
    private void promptSpeechInput() {
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT,"Teste Api de Voz Android");

        try {
            startActivityForResult(i, 100);


        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainActivity.this, "Desculpe! Deu erro",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int request_code, int result_code, Intent i){

        super.onActivityResult(request_code, result_code, i);

        switch (request_code) {
            case 100: if(result_code == RESULT_OK && i != null) {
                ArrayList<String> result = i.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edtDoMeio.setText(result.get(0));
            }
                break;
        }
    }

    public void onPause() {
        if(falador == null) {
            falador.stop();
            falador.shutdown();
        }
        super.onPause();
    }
     **/

    //Util
    private void callDiallog(String mensage, final String[] permission) {

        new AlertDialog.Builder(this)
                .setTitle("Permission")
                .setMessage(mensage)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this,permission,REQUEST_PERMISSION_CODE);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

}
